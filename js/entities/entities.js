/*global game, me, console */

/* ------------------- 
a player entity
-------------------------------- */
game.PlayerEntity = me.ObjectEntity.extend({

    /* -----
    constructor
    ------ */
    init: function (x, y, settings) {
        "use strict";
        
        // call the constructor
        this.parent(x, y, settings);
       // set the default horizontal & vertical speed (accel vector)
        this.setVelocity(3, 15);
        
        // adjust bounding box
        this.updateColRect(8, 48, -1, 0);
    
        // set the display to follow our position on both axis
        me.game.viewport.follow(this.pos, me.game.viewport.AXIS.BOTH);
    },
    
    /* -----
    update the player pos
    ------ */
    update: function () {
        "use strict";
            
        if (me.input.isKeyPressed('left')) {
            // flip the sprite on horizontal axis
            this.flipX(true);
            // update the entity velocity
            this.vel.x -= this.accel.x * me.timer.tick;
        } else if (me.input.isKeyPressed('right')) {
            // unflip the sprite
            this.flipX(false);
            // update the entity velocity
            this.vel.x += this.accel.x * me.timer.tick;
        } else {
            this.vel.x = 0;
        }
        
        if (me.input.isKeyPressed('jump')) {
            // make sure we are not already jumping or falling
            if (!this.jumping && !this.falling) {
                // set current vel to the maximum defined value
                // gravity will then do the rest
                this.vel.y = -this.maxVel.y * me.timer.tick;
                // set the jumping flag
                this.jumping = true;
            }
        
        }
            
        // check & update player movement
        this.updateMovement();
        
        

        // check for collision
        var res = me.game.collide(this);

        if (res) {
            // if we collide with an enemy
            if (res.obj.type === me.game.ENEMY_OBJECT) {
                // check if we jumped on it
                if ((res.y > 0) && !this.jumping) {
                    // bounce (force jump)
                    this.falling = false;
                    this.vel.y = -this.maxVel.y * me.timer.tick;
                    // set the jumping flag
                    this.jumping = true;

                } else {
                    // let's flicker in case we touched an enemy
                    this.renderable.flicker(45);
                }
            }
        }

        // update animation if necessary
        if (this.vel.x !== 0 || this.vel.y !== 0) {
            // update object animation
            this.parent();
            return true;
        }

        // else inform the engine we did not perform
        // any update (e.g. position, animation)
        return false;
    }
});



/*********************************************
 * Coins
**/
game.CoinEntity = me.CollectableEntity.extend({
    
    init: function (x, y, settings) {
        "use strict";
        this.parent(x, y, settings);
    },
    
    onCollision: function () {
        "use strict";
        this.collidable = false;
        me.game.remove(this);
    }
    
});


/********************************************
 * Enemy
 */
game.EnemyEntity = me.ObjectEntity.extend({
    
    init: function (x, y, settings) {
        "use strict";
        settings.image = "wheelie_right";
        settings.spritewidth = 64;
        this.parent(x, y, settings);

        this.startX = x;
        this.endX = x + settings.width - settings.spritewidth;

        //make him starts from the right
        this.pos.x = x + settings.width - settings.spritewidth;
        this.walkLeft  = true;

        //walking and jumping speed
        this.setVelocity(4, 6);

        //make it collidable
        this.collidable = true;
        this.type = me.game.ENEMY_OBJECT;
    },

    onCollision: function (res, obj) {
        "use strict";
        if (this.alive && (res.y > 0) && obj.falling) {
            this.renderable.flicker(45);
        }
    },

    update: function () {
        "use strict";
        
        // do nothing if not in viewport
        if (!this.inViewport || !this.alive) {
            return false;
        }

        if (this.alive) {
            if (this.walkLeft && this.pos.x <= this.startX) {
                this.walkLeft = false;
            } else if (!this.walkLeft && this.pos.x >= this.endX) {
                this.walkLeft = true;
            }
            // make it walk
            this.flipX(this.walkLeft);
            this.vel.x += (this.walkLeft) ? -this.accel.x * me.timer.tick : this.accel.x * me.timer.tick;

        } else {
            this.vel.x = 0;
        }

        // check and update movement
        this.updateMovement();

        // update animation if necessary
        if (this.vel.x !== 0 || this.vel.y !== 0) {
            // update object animation
            this.parent();
            return true;
        }
        return false;
    }

});

/*********************************************
 * Script
**/
game.ScriptEntity = me.ObjectEntity.extend({

    /**
        ideas : 
        * generate a custom map => event handling 
        
        * having a "cursor" : 
            - first clic on a square : select this square
            - second clic : posing the element
            
        * have an array of next elements showing in the GUI
        * when posing an element : 
            - remove it from the array 
            - add it to the map
            - check neighbors
    
    */
    init: function (x, y, settings) {
        "use strict";
        this.parent(x, y, settings);
        
     
    },
    
  
    
    update: function () {
        "use strict";
        /*
        game.data.ttimer -= me.timer.tick;
        console.log(game.data.ttimer);
        if (game.data.ttimer <= 0) {
            
            var cols = me.game.currentLevel.cols,
                rows = me.game.currentLevel.rows,
                x = Math.floor(Math.random() * cols),
                y = Math.floor(Math.random() * rows);
            me.game.currentLevel.getLayerByName("MainLayer").setTile(x, y, 2);
            
            game.data.ttimer = 2;
        }
        */
        this.parent();
        return false;
    }
});

/*********************************************
 * CursorEntity
**/
game.CursorEntity = me.ObjectEntity.extend({
    
    init: function (x, y, settings) {
        "use strict";
        this.parent(x, y, settings);
        
        this.tile = new me.Vector2d(0, 0);
        
        me.input.registerPointerEvent("mouseup", me.game.viewport, this.clicked.bind(this));
        // set the display to follow our position on both axis   
        me.game.viewport.follow(this.pos, me.game.viewport.AXIS.BOTH);
    },
    
    clicked: function (event) {
        "use strict";
        var pos, targetPos, tween, layer, i, j, tileId,
            tile = new me.Vector2d();
        layer = game.data.mainLayer;
        
        if (me.input.touches !== undefined) {
            pos = me.input.touches[0];
        } else {
            pos = me.input.mouse.pos;
        }
        pos = me.game.viewport.localToWorld(pos.x, pos.y);
        pos = pos.floorSelf();
        tile.x = pos.x / layer.tilewidth;
        tile.y = pos.y / layer.tileheight;
        tile = tile.floorSelf();
        
        if (this.tile.x !== tile.x || this.tile.y !== tile.y) {
            
            this.tile = tile;
            targetPos = new me.Vector2d(tile.x * layer.tilewidth, tile.y * layer.tileheight);
            
            tween = new me.Tween(this.pos).to(targetPos, 300);
            tween.easing(me.Tween.Easing.Cubic.InOut);
            tween.start();
        } else {
            // TODO : second clic => drop bloc
            me.game.viewport.shake(10, 100);
            for (i = -1; i <= 1; i += 1) {
                for (j = -1; j <= 1; j += 1) {
                    tileId = layer.getTileId(pos.x + i * layer.tilewidth, pos.y + j * layer.tileheight);
                    game.pushCurrentBlock(tile.x + i, tile.y + j, tileId);
                }
            }
            game.data.nextBlock = game.getRandomBlockGid();
            
            me.game.sort();
        }
    }
    
});

