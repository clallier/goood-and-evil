/*global game, me, console*/

/**
 * a HUD container and child items
 */

game.HUD = game.HUD || {};


game.HUD.Container = me.ObjectContainer.extend({

	init: function () {
        "use strict";
        
		// call the constructor
		this.parent();
		
		// persistent across level change
		this.isPersistent = true;
		
		// non collidable
		this.collidable = false;
		
		// make sure our object is always draw first
		this.z = Infinity;

		// give a name
		this.name = "HUD";
        
        //store next block
		this.nextBlock = game.data.nextBlock;
        this.blockSprite = new me.SpriteObject(576, 396, game.getBlockSprite(this.nextBlock), 64, 64);
		this.blockSprite.floating = true;
        
		// add our child score object at the top left corner
        this.addChild(this.blockSprite);
        //this.addChild(new game.HUD.ScoreItem(630, 440));
	},
    
    
	/**
	 * update function
	 */
	update : function () {
        "use strict";
		// we don't do anything fancy here, so just
		// return true if the score has been updated
		if (this.nextBlock !== game.data.nextBlock) {
			this.nextBlock = game.data.nextBlock;
            this.removeChild(this.blockSprite);
            this.blockSprite = null;
			this.blockSprite = new me.SpriteObject(576, 396, game.getBlockSprite(this.nextBlock), 64, 64);
            this.blockSprite.floating = true;
			this.addChild(this.blockSprite);
            return true;
		}
		return false;
	}

    
});


/** 
 * a basic HUD item to display score
 */
game.HUD.ScoreItem = me.Renderable.extend({
	/** 
	 * constructor
	 */
	init: function (x, y) {
		"use strict";
        
		// call the parent constructor 
		// (size does not matter here)
		this.parent(new me.Vector2d(x, y), 10, 10);
		
        //create a font 
        this.font = new me.BitmapFont("32x32_font", 32);
        this.font.set("right");
        
		// local copy of the global score
		this.nextBlock = game.data.nextBlock;
		this.blockName = game.getBlockName(this.nextBlock);

		// make sure we use screen coordinates
		this.floating = true;
	},

	/**
	 * update function
	 */
	update : function () {
        "use strict";
		// we don't do anything fancy here, so just
		// return true if the score has been updated
		if (this.nextBlock !== game.data.nextBlock) {
			this.nextBlock = game.data.nextBlock;
			this.blockName = game.getBlockName(this.nextBlock);
			return true;
		}
		
		return false;
	},

	/**
	 * draw the score
	 */
	draw : function (context) {
		"use strict";
        console.log(this.nextBlock + ", " + this.blockName);
        this.font.draw(context, "0.223", this.pos.x, this.pos.y);
	}
});