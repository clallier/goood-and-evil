/*global game*/
game.resources = [

	/* Graphics. 
	 * @example
	 * {name: "example", type:"image", src: "data/img/example.png"},
	 */
	{name: "area01_level_tiles",    type: "image", src: "data/img/map/area01_level_tiles.png"},
    {name: "metatiles32x32",        type: "image", src: "data/img/map/metatiles32x32.png"},
	{name: "gripe_run_right",       type: "image", src: "data/img/sprite/gripe_run_right.png"},
	{name: "area01_bkg0",           type: "image", src: "data/img/area01_bkg0.png"},
    {name: "area01_bkg1",           type: "image", src: "data/img/area01_bkg1.png"},
    {name: "spinning_coin_gold",    type: "image", src: "data/img/sprite/spinning_coin_gold.png"},
    {name: "wheelie_right",         type: "image", src: "data/img/sprite/wheelie_right.png"},
    {name: "tileset",               type: "image", src: "data/img/map/tileset.png"},
    {name: "cursor",                type: "image", src: "data/img/sprite/cursor.png"},
    {name: "cereals",               type: "image", src: "data/img/sprite/cereals.png"},
    {name: "earth",                 type: "image", src: "data/img/sprite/earth.png"},
    {name: "mountain",              type: "image", src: "data/img/sprite/mountain.png"},
    {name: "tree",                  type: "image", src: "data/img/sprite/tree.png"},
    {name: "water",                 type: "image", src: "data/img/sprite/water.png"},
    {name: "sand",                  type: "image", src: "data/img/sprite/sand.png"},
    // game font
    {name: "32x32_font",            type: "image", src: "data/img/font/32x32_font.png"},
 
    /* Atlases 
	 * @example
	 * {name: "example_tps", type: "tps", src: "data/img/example_tps.json"},
	 */
	/* Maps. 
	 * @example
	 * {name: "example01", type: "tmx", src: "data/map/example01.tmx"},
	 * {name: "example01", type: "tmx", src: "data/map/example01.json"},
     */
    {name: "area1", type: "tmx", src: "data/map/area1.tmx"},
    {name: "godmap", type: "tmx", src: "data/map/godmap.tmx"}

	/* Background music. 
	 * @example
	 * {name: "example_bgm", type: "audio", src: "data/bgm/", channel : 1},
	 */

	/* Sound effects. 
	 * @example
	 * {name: "example_sfx", type: "audio", src: "data/sfx/", channel : 2}
	 */
];
