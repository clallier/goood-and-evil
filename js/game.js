/*global document, window, debugPanel, me, console, alert, MapEntity */

/* Game namespace */
var game;
game = {

    // an object where to store game information
    data: {
        // score
        score: 0,
        ttimer: 0,
        mainLayer: null,
        nextBlock: 0 //TODO : list of the next elements 
    },
    
    blockType: {
        earth :     {value: 0, name: "earth",       gid: 1},
        cereals:    {value: 1, name: "cereals",     gid: 2},
        water :     {value: 2, name: "water",       gid: 8},
        sand :      {value: 3, name: "sand",        gid: 9},
        tree :      {value: 4, name: "tree",        gid: 10},
        mountain :  {value: 5, name: "mountain",    gid: 11}
    },
    
    // return the image from the block gid
    getBlockSprite: function (blockGid) {
        "use strict";
        var t, currentType;
        for (t in game.blockType) {
            currentType = game.blockType[t]; // Get the object mapped to the name in t
            if (blockGid === currentType.gid) {
                return me.loader.getImage(currentType.name);
            }
        }
    },
    
    // return a random gid
    getRandomBlockGid: function () {
        "use strict";
        var t, currentType, i = Math.floor(Math.random() * 6);
        for (t in game.blockType) {
            currentType = game.blockType[t]; // Get the object mapped to the name in t
            if (i === currentType.value) {
                return currentType.gid;
            }
        }
    },
    
    // return name from block gid
    getBlockName: function (blockGid) {
        "use strict";
        var t, currentType;
        for (t in game.blockType) {
            currentType = game.blockType[t]; // Get the object mapped to the name in t
            if (blockGid === currentType.gid) {
                return currentType.name;
            }
        }
    },
    
    // return value from block gid
    getBlockValue: function (blockGid) {
        "use strict";
        var t, currentType;
        for (t in game.blockType) {
            currentType = game.blockType[t]; // Get the object mapped to the name in t
            if (blockGid === currentType.gid) {
                return currentType.value;
            }
        }
    },

    /**
     * Integrate current block to the map 
     */
    pushCurrentBlock: function (xTile, yTile, oldGid) {
        "use strict";
        var layer = game.data.mainLayer,
            newGid = game.transformBlock(oldGid, game.data.nextBlock);
        layer.setTile(xTile, yTile, newGid);
    },

    transformBlock: function (block1, block2) {
        "use strict";
        var t = game.blockType,
            b1 = game.getBlockValue(block1),
            b2 = game.getBlockValue(block2),
            matrix = [
  //            earth         cereals     water       sand        tree        mountain
  /* earth */   [t.earth,     t.earth,    t.sand,     t.earth,    t.earth,    t.mountain],
  /* cereals */ [t.cereals,   t.cereals,  t.water,    t.sand,     t.cereals,  t.mountain],
  /* water */   [t.water,     t.cereals,  t.water,    t.water,    t.tree,     t.earth],
  /* sand */    [t.sand,      t.sand,     t.sand,     t.sand,     t.sand,     t.mountain],
  /* tree */    [t.tree,      t.tree,     t.water,    t.earth,    t.tree,     t.earth],
  /* mountain */[t.mountain,  t.mountain, t.earth,    t.mountain, t.mountain, t.mountain]];
        return matrix[b2][b1].gid;
    },

    // Run on page load.
    "onload": function () {
        "use strict";
        
        // Initialize the video.
        if (!me.video.init("screen", 640, 480, true, "auto")) {
            alert("Your browser does not support HTML5 canvas.");
            return;
        }

        // add "#debug" to the URL to enable the debug Panel
        if (document.location.hash === "#debug") {
            window.onReady(function () {
                me.plugin.register.defer(debugPanel, "debug");
            });
        }

        // Initialize the audio.
        me.audio.init("mp3, ogg");

        // Set a callback to run when loading is complete.
        me.loader.onload = this.loaded.bind(this);

        // Load the resources.
        me.loader.preload(game.resources);

        // Initialize melonJS and display a loading screen.
        me.state.change(me.state.LOADING);
    },

    // Run on game resources loaded.
    "loaded": function () {
        "use strict";
        me.debug.renderHitBox = true;
        me.sys.gravity = 0;

        me.state.set(me.state.PLAY, new game.PlayScreen());

        //entity pool
        me.entityPool.add("mainPlayer", game.PlayerEntity);
        me.entityPool.add("CoinEntity", game.CoinEntity);
        me.entityPool.add("EnemyEntity", game.EnemyEntity);
        me.entityPool.add("ScriptEntity", game.ScriptEntity);
        me.entityPool.add("CursorEntity", game.CursorEntity);
        
        //enable the keyboard
        me.input.bindKey(me.input.KEY.LEFT, 'left');
        me.input.bindKey(me.input.KEY.RIGHT, 'right');
        me.input.bindKey(me.input.KEY.X, 'jump', true);

        // Start the game.
        me.state.change(me.state.PLAY);
    }
};
