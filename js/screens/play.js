/*global me, console, game*/

game.PlayScreen = me.ScreenObject.extend({

    init: function () {
        "use strict";
        this.frameCount = 0;
    },
    
    
    // update function
    update: function () {
        "use strict";
        this.frameCount += 1;
        console.log(this.frameCount); // The log actually shows how the frameCount ranges from 1 to 45 as expected
        game.data.ttimer -= me.timer.tick();
        return this.parent();
    },
    
    /**
	 *  action to perform on state change
	 */
    onResetEvent: function () {
        "use strict";
        
		//load a level 
        me.levelDirector.loadLevel("godmap");
		//me.levelDirector.loadLevel("area1");
	
		// reset the score
		game.data.score = 0;
        game.data.ttimer = 2;
        game.data.mainLayer = me.game.currentLevel.getLayerByName("MainLayer");
        game.data.nextBlock = 2;


		// add our HUD to the game world
		this.HUD = new game.HUD.Container();
		me.game.world.addChild(this.HUD);
	},


	/**
	 *  action to perform when leaving this screen (state change)
	 */
	onDestroyEvent: function () {
        "use strict";
		// remove the HUD from the game world
		me.game.world.removeChild(this.HUD);
	}
});
